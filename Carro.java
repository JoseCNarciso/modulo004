public class Carro  {


    public static final String VERMELHO = "Vermelho";
    public static final String PRETA = "Preta";
    private Integer quantidadePneus;
    private Integer quantidadeCalotas;
    private Integer quantidadeParafuso;
    private String cor = "Verde neom" ;
    private int quantidadePortas = 5;
    private int numeroChassis = 1987295841;
    private double potenciaMotor = 2.0;
    private String packSeguranca = "Pack premeium";
    private String acabamentoInterno = "Pack Luxo";
    private int anoFabricacao =  2022;
    private String anoModelo = "modelo 2022/2022";
    private String placaVeiculo = " REAJ6B15";










    public Carro(Integer quantidadePneus) {
        setQuantidadePneus(quantidadePneus);
    }
    public Integer getQuantidadePneus() {
        return quantidadePneus + 1;

    }
    public void  setQuantidadePneus(Integer quantidadePneus) {
       setQuantidadeParafuso(quantidadePneus * 5);
        setQuantidadeCalotas(quantidadePneus + 1);

        this.quantidadePneus = quantidadePneus;

    }

    public String getCor() {
        return cor;
    }

    public void setCor( String cor) {
        this.cor = cor;
    }

    public Integer getQuantidadeCalotas() {
        return quantidadeCalotas;
    }

    public void setQuantidadeCalotas( Integer quantidadeCalotas ) {
        this.quantidadeCalotas = quantidadeCalotas;
    }

    public Integer getQuantidadeParafuso() {
        return quantidadeParafuso;
    }

    public void setQuantidadeParafuso( Integer quantidadeParafuso ) {
        this.quantidadeParafuso = quantidadeParafuso;
    }

    public int getQuantidadePortas() {

        return quantidadePortas;
    }

    public void setQuantidadePortas( int quantidadePortas ) {



        this.quantidadePortas = quantidadePortas ;
    }

    public int getNumeroChassis() {
        return numeroChassis;
    }

    public void setNumeroChassis( int numeroChassis ) {
        this.numeroChassis = numeroChassis;
    }

    public double getPotenciaMotor() {
        return potenciaMotor;
    }

    public void setPotenciaMotor( double potenciaMotor ) {
        this.potenciaMotor = potenciaMotor;
    }

    public String getPackSeguranca() {
        return packSeguranca;
    }

    public void setPackSeguranca( String packSeguranca ) {
        this.packSeguranca = packSeguranca;
    }

    public String getAcabamentoInterno() {
        return acabamentoInterno;
    }

    public void setAcabamentoInterno( String acabamentoInterno ) {
        this.acabamentoInterno = acabamentoInterno;
    }

    public int getAnoFabricacao() {
        return anoFabricacao;
    }

    public void setAnoFabricacao( int anoFabricacao ) {
        this.anoFabricacao = anoFabricacao;
    }

    public String getAnoModelo() {
        return anoModelo;
    }

    public void setAnoModelo( String anoModelo ) {
        this.anoModelo = anoModelo;
    }

    public String getPlacaVeiculo() {
        return placaVeiculo;
    }

    public void setPlacaVeiculo( String placaVeiculo ) {

        this.placaVeiculo = placaVeiculo;
    }



    public void imprimeValores() {
        System.out.println("Quantidade de pneus: " + getQuantidadePneus());
        System.out.println("Quantidae de calotas: " + getQuantidadeCalotas() );
        System.out.println("Quantidade de parafusos: " + getQuantidadeParafuso());
        System.out.println("Cor do veículo: " + getCor());
        System.out.println("quantidade de portas: " + getQuantidadePortas());
        System.out.println("Placa do veiculo: " + getPlacaVeiculo());
        System.out.println("Acabamento interno do veiculo:" + getAcabamentoInterno());
        System.out.println("Número do chassis do veículo: " + getNumeroChassis());
        System.out.println("Potência do motor:" + getPotenciaMotor());
        System.out.println("Ano de fabricação do veículo: " + getAnoFabricacao());
        System.out.println("Modelo do veículo: " + getAnoModelo());
        System.out.println("Pacote de segurança: " + getPackSeguranca());
        System.out.println("Cor do veículo: " + getCor());




    }


}
